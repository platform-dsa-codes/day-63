import java.util.*;

class LRUCache {
    class ListNode {
        int key;
        int value;
        ListNode prev;
        ListNode next;
        
        public ListNode(int key, int value) {
            this.key = key;
            this.value = value;
        }
    }
    
    private Map<Integer, ListNode> cache;
    private int capacity;
    private ListNode head;
    private ListNode tail;

    public LRUCache(int capacity) {
        this.capacity = capacity;
        cache = new HashMap<>();
        head = new ListNode(-1, -1);
        tail = new ListNode(-1, -1);
        head.next = tail;
        tail.prev = head;
    }
    
    public int get(int key) {
        if (!cache.containsKey(key))
            return -1;
        
        ListNode node = cache.get(key);
        // Move accessed node to the front
        moveToHead(node);
        return node.value;
    }
    
    public void put(int key, int value) {
        if (cache.containsKey(key)) {
            // Update the value
            ListNode node = cache.get(key);
            node.value = value;
            // Move the updated node to the front
            moveToHead(node);
        } else {
            // Add new node to the cache
            ListNode newNode = new ListNode(key, value);
            cache.put(key, newNode);
            // Add new node to the front
            addToFront(newNode);
            // Check if capacity is exceeded, if yes, remove the least recently used node
            if (cache.size() > capacity) {
                ListNode tailNode = removeTail();
                cache.remove(tailNode.key);
            }
        }
    }
    
    private void moveToHead(ListNode node) {
        removeNode(node);
        addToFront(node);
    }
    
    private void removeNode(ListNode node) {
        node.prev.next = node.next;
        node.next.prev = node.prev;
    }
    
    private void addToFront(ListNode node) {
        node.next = head.next;
        head.next.prev = node;
        head.next = node;
        node.prev = head;
    }
    
    private ListNode removeTail() {
        ListNode tailNode = tail.prev;
        removeNode(tailNode);
        return tailNode;
    }
}

/*
LRUCache obj = new LRUCache(2);
obj.put(1, 1);
obj.put(2, 2);
System.out.println(obj.get(1)); // Output: 1
obj.put(3, 3);
System.out.println(obj.get(2)); // Output: -1
obj.put(4, 4);
System.out.println(obj.get(1)); // Output: -1
System.out.println(obj.get(3)); // Output: 3
System.out.println(obj.get(4)); // Output: 4
 */
