import java.util.*;

class BrowserHistory {
    private List<String> history;
    private int currentIndex;

    public BrowserHistory(String homepage) {
        history = new ArrayList<>();
        history.add(homepage);
        currentIndex = 0;
    }
    
    public void visit(String url) {
        // Clear forward history
        while (history.size() > currentIndex + 1) {
            history.remove(history.size() - 1);
        }
        history.add(url);
        currentIndex++;
    }
    
    public String back(int steps) {
        int newIndex = Math.max(0, currentIndex - steps);
        currentIndex = newIndex;
        return history.get(currentIndex);
    }
    
    public String forward(int steps) {
        int newIndex = Math.min(history.size() - 1, currentIndex + steps);
        currentIndex = newIndex;
        return history.get(currentIndex);
    }
}

/*
BrowserHistory obj = new BrowserHistory("leetcode.com");
obj.visit("google.com");
obj.visit("facebook.com");
obj.visit("youtube.com");
System.out.println(obj.back(1)); // Output: facebook.com
System.out.println(obj.back(1)); // Output: google.com
System.out.println(obj.forward(1)); // Output: facebook.com
obj.visit("linkedin.com");
System.out.println(obj.forward(2)); // Output: linkedin.com
System.out.println(obj.back(2)); // Output: google.com
System.out.println(obj.back(7)); // Output: leetcode.com
*/
